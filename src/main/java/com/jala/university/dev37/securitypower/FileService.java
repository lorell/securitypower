package com.jala.university.dev37.securitypower;

public interface FileService<SelfType> {

  void write(SelfType type);
  SelfType read();
}
