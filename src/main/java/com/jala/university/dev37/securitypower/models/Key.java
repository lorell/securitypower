package com.jala.university.dev37.securitypower.models;

public class Key extends SensitiveInfo {

  private String serial;

  public Key() {
    super();
  }

  public String getSerial() {
    return serial;
  }

  public void setSerial(String serial) {
    this.serial = serial;
  }

  @Override
  protected SensitiveInfoType getType() {
    return SensitiveInfoType.KEY;
  }
}
