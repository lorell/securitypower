package com.jala.university.dev37.securitypower.models;

public class Note extends SensitiveInfo {
  private String note;

  public Note() {
    super();
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  @Override
  protected SensitiveInfoType getType() {
    return SensitiveInfoType.NOTE;
  }
}
