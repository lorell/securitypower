package com.jala.university.dev37.securitypower.configs;

import com.jala.university.dev37.securitypower.AddCommand;
import com.jala.university.dev37.securitypower.Command;
import com.jala.university.dev37.securitypower.CommandExecutor;
import com.jala.university.dev37.securitypower.UpdateCommand;
import java.util.Arrays;

public enum ActionCommand {
  ADD("add", AddCommand.class),
  UPDATE("update", UpdateCommand.class),
  REMOVE("rm", null),
  FIND("find", null),
  LIST("list", null);

  private String value;
  private Class<? extends CommandExecutor> commandExecutor;

  ActionCommand(String value, Class<? extends CommandExecutor> commandExecutor) {
    this.value = value;
    this.commandExecutor = commandExecutor;
  }

  public String getValue() {
    return value;
  }

  public static ActionCommand getActionCommand(String value) {
    return Arrays.stream(values()).filter(current -> current.getValue().equals(value)).findFirst().orElse(null);
  }

  public Class<? extends CommandExecutor> getCommandExecutor() {
    return commandExecutor;
  }
}
