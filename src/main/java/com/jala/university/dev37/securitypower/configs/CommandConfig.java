package com.jala.university.dev37.securitypower.configs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandConfig {

  private static final List<Option> REMOVE_OPTIONS = new ArrayList<>();
  private static final List<Option> ADD_OPTIONS = new ArrayList<>();
  private static final Map<ActionCommand, List<Option>> APP_PARAMETERS = new HashMap<>();

  static {
    ADD_OPTIONS.add(Option.TYPE);
    ADD_OPTIONS.add(Option.NAME);
    ADD_OPTIONS.add(Option.CONTAINER);
    ADD_OPTIONS.add(Option.FAVORITE);
    REMOVE_OPTIONS.add(Option.TYPE);
    REMOVE_OPTIONS.add(Option.NAME);
    REMOVE_OPTIONS.add(Option.CONTAINER);
    APP_PARAMETERS.put(ActionCommand.ADD, ADD_OPTIONS);
    APP_PARAMETERS.put(ActionCommand.REMOVE, REMOVE_OPTIONS);
    APP_PARAMETERS.put(ActionCommand.UPDATE, null);
  }

  public static final CommandConfig APP_CONFING = new CommandConfig("app", APP_PARAMETERS);

  private String startKey;
  private Map<ActionCommand, List<Option>> parameters;

  CommandConfig(String startKey, Map<ActionCommand, List<Option>> parameters) {
    this.startKey = startKey;
    this.parameters = parameters;
  }

  public String getStartKey() {
    return startKey;
  }

  public void setStartKey(String startKey) {
    this.startKey = startKey;
  }

  public Map<ActionCommand, List<Option>> getParameters() {
    return parameters;
  }

  public void setParameters(
      Map<ActionCommand, List<Option>> parameters) {
    this.parameters = parameters;
  }
}
