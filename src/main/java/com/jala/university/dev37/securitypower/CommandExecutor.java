package com.jala.university.dev37.securitypower;

import com.jala.university.dev37.securitypower.configs.ActionCommand;
import com.jala.university.dev37.securitypower.configs.CommandConfig;
import com.jala.university.dev37.securitypower.configs.Option;
import java.util.Map;

public interface CommandExecutor extends Command {

  void setCommandConfing(CommandConfig config);
  void setCurrentCommand(ActionCommand command);
  void setParameters(Map<Option, String> parameters);

}
