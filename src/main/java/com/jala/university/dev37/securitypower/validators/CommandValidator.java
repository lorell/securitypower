package com.jala.university.dev37.securitypower.validators;

import com.jala.university.dev37.securitypower.configs.ActionCommand;
import com.jala.university.dev37.securitypower.configs.CommandConfig;
import com.jala.university.dev37.securitypower.configs.Option;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import org.apache.commons.collections4.CollectionUtils;

public class CommandValidator implements Validator {
  private String command;
  private CommandConfig config;

  private ActionCommand actionCommand;
  private Map<Option, String> parameters;

  public CommandValidator(CommandConfig config) {
    this.config = config;
    parameters = new HashMap<>();
  }

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command.trim();
  }

  @Override
  public boolean valid() {
    boolean isCommandValid = false;
    if (command.startsWith(config.getStartKey())) {
      String parameters = command.substring(config.getStartKey().length()).trim();
      ActionCommand actionCommand = ActionCommand.getActionCommand(parameters.substring(0, command.indexOf(" ")).trim());
      if (config.getParameters().keySet().contains(actionCommand)) {
        this.actionCommand = actionCommand;
        isCommandValid = hasValidParameters(parameters, actionCommand);
      }
    }
    return isCommandValid;
  }

  private boolean hasValidParameters(String parameters, ActionCommand actionCommand) {
    boolean isCommandValid = true;
    List<String> options = Arrays.asList(
        parameters.substring(actionCommand.getValue().length()).trim().split(" "));
    List<Option> validOptionsByCommand = config.getParameters().get(this.actionCommand);
    if (options.size() == 1) {
      isCommandValid = validOptionsByCommand.isEmpty();
    } else {
      isCommandValid = checkOptions(isCommandValid, options, validOptionsByCommand);
    }
    return isCommandValid;
  }

  private boolean checkOptions(boolean isCommandValid, List<String> options,
      List<Option> validOptionsByCommand) {
    Iterator<String> iterator = options.iterator();
    while (iterator.hasNext() && isCommandValid) {
      String option = iterator.next();
      Option validOption = getOption(validOptionsByCommand, option);
      String value = iterator.next();
      if (Objects.nonNull(validOption) && hasValidValue(validOption, value)) {
          this.parameters.put(validOption, value);
      } else {
        isCommandValid = false;
      }
    }
    return isCommandValid;
  }

  private boolean hasValidValue(Option option, String value) {
    return (CollectionUtils.isNotEmpty(option.getValidValues()) && option.getValidValues().contains(value))
        || (option.getPattern() != null && option.getPattern().matcher(value).matches());
  }
  private Option getOption(List<Option> validOptionsByCommand, String option) {
    return validOptionsByCommand.stream().filter(current -> current.getKey().equals(option)).findFirst().orElse(null);
  }

  private boolean isValidOption(List<Option> validOptionsByCommand, String option) {
    return validOptionsByCommand.stream().anyMatch(current -> current.getKey().equals(option));
  }

  public CommandConfig getConfig() {
    return config;
  }

  public void setConfig(CommandConfig config) {
    this.config = config;
  }

  public ActionCommand getActionCommand() {
    return actionCommand;
  }

  public Map<Option, String> getParameters() {
    return parameters;
  }
}
