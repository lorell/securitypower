package com.jala.university.dev37.securitypower.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.collections4.CollectionUtils;

public abstract class SensitiveInfo {
  private final UUID uuid;
  private String name;
  private String container;
  private boolean favorite;
  private Set<String> tags;
  private SensitiveInfoType type;

  public SensitiveInfo() {
    this.uuid = UUID.randomUUID();
    this.type = getType();
  }

  protected abstract SensitiveInfoType getType();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContainer() {
    return container;
  }

  public void setContainer(String container) {
    this.container = container;
  }

  public boolean isFavorite() {
    return favorite;
  }

  public void setFavorite(boolean favorite) {
    this.favorite = favorite;
  }

  public Set<String> getTags() {
    return tags;
  }

  public void setTags(Set<String> tags) {
    this.tags = tags;
  }

  public void addTag(String tag) {
    if (CollectionUtils.isEmpty(this.tags)) {
      this.tags = new HashSet<>();
    }
    this.tags.add(tag);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SensitiveInfo that = (SensitiveInfo) o;
    return uuid.equals(that.uuid) && type.equals(that.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid, type);
  }
}
