package com.jala.university.dev37.securitypower;

import com.jala.university.dev37.securitypower.configs.CommandConfig;
import com.jala.university.dev37.securitypower.validators.CommandValidator;
import java.lang.reflect.InvocationTargetException;

public class Main {

  public static void main(String[] args)
      throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
    System.out.println("Hello world!");
    String command = "app add -t note -c tests";
    CommandValidator commandValidator = new CommandValidator(CommandConfig.APP_CONFING);
    commandValidator.setCommand(command);
    CommandProcessor processor = new CommandProcessor(commandValidator);
    processor.execute();
    commandValidator.setCommand("app add -t key -c hola");
    processor.execute();
    //System.out.println(isValid);
  }
}