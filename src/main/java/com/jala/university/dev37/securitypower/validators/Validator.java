package com.jala.university.dev37.securitypower.validators;

@FunctionalInterface
public interface Validator {
  boolean valid();
}
