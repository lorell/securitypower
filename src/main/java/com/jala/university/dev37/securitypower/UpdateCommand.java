package com.jala.university.dev37.securitypower;

import com.jala.university.dev37.securitypower.configs.ActionCommand;
import com.jala.university.dev37.securitypower.configs.CommandConfig;
import com.jala.university.dev37.securitypower.configs.Option;
import java.util.Map;

public class UpdateCommand implements CommandExecutor{

  @Override
  public void execute() {

  }

  @Override
  public void setCommandConfing(CommandConfig config) {

  }

  @Override
  public void setCurrentCommand(ActionCommand command) {

  }

  @Override
  public void setParameters(Map<Option, String> parameters) {

  }
}
