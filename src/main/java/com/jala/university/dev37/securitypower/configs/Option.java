package com.jala.university.dev37.securitypower.configs;

import com.jala.university.dev37.securitypower.models.SensitiveInfoType;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Option {

  public static final Option TYPE = new Option("-t", null,
      Arrays.stream(SensitiveInfoType.values()).map(type -> type.getTypeString()).toList());
  public static final Option NAME = new Option("-n", Pattern.compile("\\w*"), null);
  public static final Option CONTAINER = new Option("-c", Pattern.compile("\\w*"), null);
  public static final Option FAVORITE = new Option("-f", null, Arrays.asList(Boolean.FALSE.toString(), Boolean.TRUE.toString())) {
    @Override
    public boolean needValue() {
      return false;
    }
  };

  private final String key;
  private final Pattern pattern;
  private final List<String> validValues;

  private Option(String key, Pattern pattern, List<String> validValues) {
    this.key = key;
    this.pattern = pattern;
    this.validValues = validValues;
  }

  public String getKey() {
    return key;
  }

  public Pattern getPattern() {
    return pattern;
  }

  public List<String> getValidValues() {
    return validValues;
  }

  public boolean needValue() {
    return true;
  }
}
