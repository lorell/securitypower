package com.jala.university.dev37.securitypower;

import com.jala.university.dev37.securitypower.configs.ActionCommand;
import com.jala.university.dev37.securitypower.configs.CommandConfig;
import com.jala.university.dev37.securitypower.configs.Option;
import com.jala.university.dev37.securitypower.models.SensitiveInfo;
import com.jala.university.dev37.securitypower.models.SensitiveInfoType;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddCommand implements CommandExecutor {
  private static final Logger LOGGER = Logger.getLogger(ActionCommand.class.getName());

  List<SensitiveInfo> info;
  private CommandConfig config;
  private Map<Option, String> parameters;
  private ActionCommand currentCommand;

  public AddCommand() {
    this.info = new ArrayList<>();
  }

  @Override
  public void execute() {
    //Create the sensitive info
    String type = parameters.get(Option.TYPE);
    try {
      SensitiveInfo info = SensitiveInfoType.getInfoTypeClassByTypeValue(type).getConstructor(null).newInstance(null);
      if (parameters.containsKey(Option.CONTAINER)) {
        info.setContainer(parameters.get(Option.CONTAINER));
      }
      //TODO: Add a in memory persistence with a service, may should be a controller that receive a command and a service
      // Temporal persistence
      this.info.add(info);
    } catch (InstantiationException e) {
      LOGGER.log(Level.WARNING, "Sensitive Info are not created");
    } catch (IllegalAccessException e) {
      LOGGER.log(Level.WARNING, "Sensitive Info are not created");
    } catch (InvocationTargetException e) {
      LOGGER.log(Level.WARNING, "Sensitive Info are not created");
    } catch (NoSuchMethodException e) {
      LOGGER.log(Level.WARNING, "Sensitive Info are not created");
    }
  }

  @Override
  public void setCommandConfing(CommandConfig config) {
    this.config = config;
  }

  @Override
  public void setCurrentCommand(ActionCommand command) {
    this.currentCommand = command;
  }

  @Override
  public void setParameters(Map<Option, String> parameters) {
    this.parameters = parameters;
  }
}
