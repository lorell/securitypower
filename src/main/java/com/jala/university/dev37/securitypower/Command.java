package com.jala.university.dev37.securitypower;

import java.lang.reflect.InvocationTargetException;

public interface Command {

  void execute();

}
