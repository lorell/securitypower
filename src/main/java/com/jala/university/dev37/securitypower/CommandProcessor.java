package com.jala.university.dev37.securitypower;

import com.jala.university.dev37.securitypower.validators.CommandValidator;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandProcessor implements Command {

  private static final Logger LOGGER = Logger.getLogger(CommandProcessor.class.getName());

  private CommandValidator commandValidator;

  public CommandProcessor(CommandValidator commandValidator) {
    this.commandValidator = commandValidator;
  }

  @Override
  public void execute() {
    if (commandValidator.valid()) {
      try {
        CommandExecutor executor = commandValidator.getActionCommand().getCommandExecutor()
            .getConstructor(null).newInstance(null);
        executor.setCommandConfing(commandValidator.getConfig()); //Probably it is not necessary
        executor.setParameters(commandValidator.getParameters());
        executor.setCurrentCommand(commandValidator.getActionCommand());
        executor.execute();
      } catch (InstantiationException e) {
        throw new RuntimeException(e);
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      } catch (InvocationTargetException e) {
        throw new RuntimeException(e);
      } catch (NoSuchMethodException e) {
        throw new RuntimeException(e);
      }
    } else {
      LOGGER.log(Level.SEVERE, "Command cannot processed");
      //Here you should request a new command, set command and execute again.
    }
  }
}
