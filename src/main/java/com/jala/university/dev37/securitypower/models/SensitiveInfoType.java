package com.jala.university.dev37.securitypower.models;

import java.util.Arrays;

public enum SensitiveInfoType {
  NOTE("note", Note.class),
  KEY("key", Key.class);

  private String typeString;
  private Class<? extends SensitiveInfo> infoTypeClass;

  SensitiveInfoType(String typeString, Class<? extends SensitiveInfo> infoTypeClass) {
    this.typeString = typeString;
    this.infoTypeClass = infoTypeClass;
  }

  public Class<? extends SensitiveInfo> getInfoTypeClass() {
    return infoTypeClass;
  }

  public static Class<? extends SensitiveInfo> getInfoTypeClass(SensitiveInfoType type) {
    return Arrays.stream(SensitiveInfoType.values()).filter(infoType -> infoType == type).findFirst().get()
        .getInfoTypeClass();
  }

  public static Class<? extends SensitiveInfo> getInfoTypeClassByTypeValue(String type) {
    //Need refactor duplicated code
    return Arrays.stream(SensitiveInfoType.values()).filter(infoType -> infoType.getTypeString().equals(type)).findFirst().get()
        .getInfoTypeClass();
  }
  public String getTypeString() {
    return typeString;
  }
}
